<?php

namespace Ymkj\Quicktool;

class Config
{
    public static function get($name)
    {
        if (empty($name)) {
            return [];
        }

        return self::$list[$name] ? self::$list[$name]: [];
    }

    private static $list = [
        'tableFeildToMysqlType' => [
            'int' => 'integer',
            'tinyint' => 'tinyInteger',
            'bigint' => 'bigInteger',
            'char' => 'char',
            'varchar' => 'string',
            'decimal' => 'decimal',
            'timestamp' => 'timestamp',
            'text' => 'text',
            'datetime' => 'dateTime'
        ]
    ];
}