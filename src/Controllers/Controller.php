<?php

namespace Ymkj\Quicktool\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class Controller
 * @package Ymkj\Quicktool\Controllers
 */
class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    ///api结果格式化
    use \App\Http\Traits\ResponseTrait;
    use \App\Http\Traits\EncodeTrait;

    public function __construct() {
        $key = "121130tb92-n87df-23r4h!sdfzpofurm.w";

        /// 验证token
        if ($_SERVER['REQUEST_METHOD'] !== 'OPTIONS') {
            if (empty($_SERVER['HTTP_X_TOKEN'])) {
                exit(json_encode([
                    'code' => 10002,
                    'message' => 'invalid request1'
                ]));
            }

            $token = $_SERVER['HTTP_X_TOKEN'];
            if (empty($token)) {
                exit(json_encode([
                    'code' => 10002,
                    'message' => 'invalid request2'
                ]));
            } else {
                $arr = explode("|", $token);
                $sign = $arr[0];
                $time = $arr[1];
                $now = time();
                if ($now - $time > 300) { // 五分钟过期
                    exit(json_encode([
                        'code' => 10002,
                        'message' => 'invalid request3'
                    ]));
                }

                $mySign = md5(sprintf("%s%s%s%s", $time, $key, $time, $key));
                if ($mySign != $sign) {
                    exit(json_encode([
                        'code' => 10002,
                        'message' => 'invalid request4'
                    ]));
                }
            }
        }
    }
}
