<?php

namespace Ymkj\Quicktool\Controllers;

use Illuminate\Http\Request;
use Ymkj\Quicktool\Controllers\Controller;
use Ymkj\Quicktool\Models\ToolHelper;

/**
 * @name 开发工具
 * Class IndexController
 * @package Ymkj\Quicktool\Controllers
 *
 * @Resource("tools")
 */
class IndexController extends Controller
{
    /**
     * 创建数据表
     * @name 创建数据表
     * @Post("/lv/tools/createMysql")
     * @Versions("v1")
     */
    public function createMysql(Request $request, ToolHelper $mToolHelper)
    {
        $params = $request->all();

        if (empty($params['name'])) {
            return $this->jsonAdminResult([], 10001, "数据表名称不能为空");
        }
        if (empty($params['comment'])) {
            return $this->jsonAdminResult([], 10001, "数据表注释不能为空");
        }
        if (empty($params['struct'])) {
            return $this->jsonAdminResult([], 10001, "字段信息不能为空");
        }
        $result = $mToolHelper->createMysql($params);

        if ($result === true) {
            return $this->jsonAdminResult();
        } else {
            return $this->jsonAdminResult([], 10001, $result);
        }
    }
    
    /**
     * 编辑数据表
     * @name 编辑数据表
     * @Post("/lv/tools/editMysql")
     * @Versions("v1")
     */
    public function editMysql(Request $request, ToolHelper $mToolHelper)
    {
        $params = $request->all();

        if (empty($params['name'])) {
            return $this->jsonAdminResult([], 10001, "数据表名称不能为空");
        }
        if (empty($params['struct'])) {
            return $this->jsonAdminResult([], 10001, "字段信息不能为空");
        }
        $result = $mToolHelper->editMysql($params);

        if ($result === true) {
            return $this->jsonAdminResult();
        } else {
            return $this->jsonAdminResult([], 10001, $result);
        }
    }

    /**
     * 配置信息
     * @name 配置信息
     * @Get("/lv/tools/getConfig")
     * @Versions("v1")
     */
    public function getConfig(Request $request, ToolHelper $mToolHelper)
    {
        $tableList = $mToolHelper->getConfig();
        $keyWords = ['abstract','and','array','as','break','callable','case','catch','class','clone','const','continue','declare',
            'default','die','do','echo','else','elseif','empty','enddeclare','endfor','endforeach','endif','endswitch','endwhile','eval','exit',
            'extends','final','finally','for','foreach','function','global','goto','if','implements','include','include_once','instanceof','insteadof',
            'interface','isset','list','namespace','new','or','print','private','protected','public','require','require_once','return','static','switch',
            'throw','trait','try','unset','use','var','while','xor','abstracts','ands','arraies','ases','breaks','callables','cases','catchs','classes','clones',
            'consts','continues','declares', 'defaults','dies','dos','echos','elses','elseifs','emptys','enddeclares','endfors','endforeachs','endifs','endswitchs',
            'endwhiles','evals','exits', 'extendses','finals','finallies','fors','foreachs','functions','globals','gotos','ifs','implementses','includes','include_onces',
            'instanceofs','insteadofs', 'interfaces','issets','lists','namespaces','news','ors','prints','privates','protecteds','publics','requires','require_onces',
            'returns','statics','switchs', 'throws','traits','tries','unsets','uses','vars','whiles','xors'];
        return $this->jsonAdminResult([
            'list' => $tableList,
            'keyWords' => $keyWords
        ]);
    }

    /**
     * 获取表具体信息
     * @name 获取表具体信息
     * @Get("/lv/tools/getTable")
     * @Versions("v1")
     */
    public function getTable(Request $request, ToolHelper $mToolHelper)
    {
        $params = $request->all();

        $data = $mToolHelper->getCreateTable($params['tableName']);
        return $this->jsonAdminResult([
            'list' => $data
        ]);
    }

    /**
     * 生成api服务
     * @name 生成api服务
     * @Post("/lv/tools/mkApiServer")
     * @Versions("v1")
     */
    public function mkApiServer(Request $request, ToolHelper $mToolHelper)
    {
        $params = $request->all();
        $mToolHelper = new ToolHelper();

        $data = [
            'client_type' => $params['client_type'], // 属于哪个端
            'parent' => ucfirst(ltrim($params['parent'], "/")),
            'parentName' => $params['parentName'],
            'name' => $params['name'],
            'path' => str_replace(".vue", "", $params['path']),
            'fields' => json_decode($params['tableInfo'], true)
        ];
        $res = $mToolHelper->refreshAdmin($data);
        if ($res['code'] != 0){
            if (strpos($res['message'], "Operation not permitted") !== false) {
                $res['message'] = '通知后端将项目改为 www 用户组和用户';
            }
            return $this->jsonAdminResult([],$res['code'],$res['message']);
        }
        $res = $mToolHelper->mkController($data);
        if ($res['code'] != 0){
            if (strpos($res['message'], "Operation not permitted") !== false) {
                $res['message'] = '通知后端将项目改为 www 用户组和用户';
            }
            return $this->jsonAdminResult([],$res['code'],$res['message']);
        }
        $res = $mToolHelper->mkModel($data);
        if ($res['code'] != 0){
            if (strpos($res['message'], "Operation not permitted") !== false) {
                $res['message'] = '通知后端将项目改为 www 用户组和用户';
            }
            return $this->jsonAdminResult([],$res['code'],$res['message']);
        }
        $res = $mToolHelper->refreshApi($data);
        if ($res['code'] != 0){
            if (strpos($res['message'], "Operation not permitted") !== false) {
                $res['message'] = '通知后端将项目改为 www 用户组和用户';
            }
            return $this->jsonAdminResult([],$res['code'],$res['message']);
        }
        return $this->jsonAdminResult();
    }
}