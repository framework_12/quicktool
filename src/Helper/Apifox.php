<?php

namespace Ymkj\Quicktool\Helper;

use Illuminate\Support\Facades\Log;

/**
 * https://apifox-openapi.apifox.cn/
 */
class Apifox
{
    // apifox开发api域名
    public static $domain = "https://api.apifox.com";

    // 项目ID
    public static $projectId;

    // 个人访问令牌
    public static $authorization;

    public function __construct($projectId, $authorization)
    {
        self::$projectId = $projectId;
        self::$authorization = $authorization;
    }

    /**
     * 创建接口
     * https://apifox-openapi.apifox.cn/api-150260514
     */
    public static function importData($path, $params=[])
    {
        if (empty($path) || empty($params)) {
            return [
                'code' => '10001',
                'message' => '创建apifox接口，参数不能为空'
            ];
        }

        $catalog = self::getCatalog($path);
        if ($catalog['code'] !== '0') {
            return $catalog;
        }

        $data = [
            'swagger' => '2.0',
            'paths' => [],
            'definitions' => []
        ];
        foreach ($params as $url => $apis) {
            $data['paths'][$url] = [];
            foreach ($apis as $method => $api) {
                $data['paths'][$url][$method] = [
                    'summary' => $api['summary'],
                    'description' => $api['description'],
                    'operationId' => "aaa",
                    'parameters' => [],
                    'responses' => []
                ];

                // parameters 参数处理
                foreach ($api['parameters'] as $type => $parameters) {
                    $schema = ltrim(str_replace("/", "_", $url), '_')."_$method";

                    if ($type == 'body') { // 请求体
                        $data['paths'][$url][$method]['parameters'][] = [
                            'in' => 'body',
                            'name' => 'body',
                            'description' => '请求体',
                            'required' => true,
                            'schema' => [
                                '$ref' => "#/definitions/$schema"
                            ]
                        ];

                        $data['definitions'][$schema] = [
                            'type' => 'object',
                            'required' => [],
                            'properties' => []
                        ];
                        foreach ($parameters as $field => $parameter) {
                            $data['definitions'][$schema]['properties'][$field] = [
                                'type' => $parameter['type'],
                                'description' => $parameter['description'],
                                'example' => $parameter['example']
                            ];
                            
                            if ($parameter['required']) {
                                $data['definitions'][$schema]['required'][] = $field;
                            }
                        }
                    } else if ($type == "query") { // query 参数
                        foreach ($parameters as $field => $parameter) {
                            $data['paths'][$url][$method]['parameters'][] = [
                                'in' => 'query',
                                'name' => $field,
                                'description' => $parameter['description'],
                                'required' => $parameter['required'],
                                'example' => $parameter['example']
                            ];
                        }
                    } else if ($type == "header") { // header 参数
                        foreach ($parameters as $field => $parameter) {
                            $data['paths'][$url][$method]['parameters'][] = [
                                'in' => 'header',
                                'name' => $field,
                                'description' => $parameter['description'],
                                'required' => $parameter['required'],
                                'example' => $parameter['example']
                            ];
                        }
                    }

                    // responses 参数处理
                    foreach ($api['responses'] as $code => $response) {
                        $data['paths'][$url][$method]['responses'][$code] = [
                            'description' => $response["description"],
                            'schema' => [
                                '$ref' => "#/definitions/{$schema}_response"
                            ]
                        ];

                        $data['definitions'][$schema."_response"] = [
                            'type' => 'object',
                            'properties' => $response['properties']
                        ];
                    }
                }
            }
        }

        $url = sprintf("%s/api/v1/projects/%s/import-data?locale=zh-CN", self::$domain, self::$projectId);
        $result = self::post($url, [
            'apiFolderId' => $catalog['id'],
            'importFormat' => 'openapi',
            'data' => json_encode($data)
        ]);
        if (! empty($result['success'])
            && ! empty($result['data'])
            && ! empty($result['data']['apiCollection'])
            && ! empty($result['data']['apiCollection']['item'])
            && $result['data']['apiCollection']['item']['createCount'] > 0
        ) {
            return [
                'code' => 0,
                'message' => 'success'
            ];
        } else {
            Log::error(sprintf("apifox接口创建失败: 请求参数 => %s; 响应结果 => %s", json_encode($data), json_encode($result)));

            return [
                'code' => 505,
                'message' => 'apifox接口创建失败'
            ];
        }

        // $data 示例
        // $data = [
        //     'swagger' => '2.0',
        //     'paths' => [
        //         '/lv/goods/add' => [
        //             'post' => [
        //                 'summary' => '添加商品',
        //                 'description' => '添加商品接口',
        //                 'operationId' => "addPet",
        //                 'parameters' => [
        //                     [
        //                         'in' => 'body',
        //                         'name' => 'body',
        //                         'description' => '123123',
        //                         'required' => true,
        //                         'schema' => [
        //                             '$ref' => '#/definitions/Goods'
        //                         ]
        //                     ],
        //                     [
        //                         'in' => 'query',
        //                         'name' => 'userId',
        //                         'description' => '用户id',
        //                         'required' => true,
        //                         'example' => '123'
        //                     ],
        //                     [
        //                         'in' => 'header',
        //                         'name' => 'X-Token',
        //                         'description' => '授权的token',
        //                         'required' => true,
        //                         'example' => '123123123|12312|12312'
        //                     ],
        //                     [
        //                         'in' => 'header',
        //                         'name' => 'device',
        //                         'description' => '客户端类型',
        //                         'required' => true,
        //                         'example' => 'windows'
        //                     ]
        //                 ],
        //                 'responses' => [
        //                     '200' => [
        //                         'description' => '响应成功',
        //                         'schema' => [
        //                             '$ref' => '#/definitions/response'
        //                         ]
        //                     ]
        //                 ]
        //             ]
        //         ]
        //     ],
        //     'definitions' => [
        //         'Goods' => [
        //             'type' => 'object',
        //             'required' => [
        //                 'title',
        //                 'imgs'
        //             ],
        //             'properties' => [
        //                 'title' => [
        //                     'type' => 'string',
        //                     'description' => '产品标题',
        //                     'example' => '科罗娜皇家至尊系列'
        //                 ],
        //                 'imgs' => [
        //                     'type' => 'array',
        //                     'description' => '图片集合',
        //                     'items' => [
        //                         'type' => 'string',
        //                         'description' => '图片地址',
        //                         'example' => 'https://image.test.com/1.jpg'
        //                     ]
        //                 ],
        //                 'status' => [
        //                     'type' => 'string',
        //                     'description' => '产品状态： 1：上架；2：下架',
        //                     'enum' => [
        //                         '1',
        //                         '2'
        //                     ]
        //                 ]
        //             ]
        //         ],
        //         'response' => [
        //             'type' => 'object',
        //             'properties' => [
        //                 'code' => [
        //                     'type' => 'integer',
        //                     'description' => '提示码',
        //                     'example' => 0
        //                 ],
        //                 'message' => [
        //                     'type' => 'string',
        //                     'description' => '提示信息',
        //                     'example' => 'success'
        //                 ],
        //                 'total' => [
        //                     'type' => 'integer',
        //                     'description' => '总条数',
        //                     'example' => 1
        //                 ],
        //                 'list' => [
        //                     'type' => 'array',
        //                     'description' => '列表数据',
        //                     'items' => [
        //                         'type' => 'object',
        //                         'description' => '数据详情',
        //                         'properties' => [
        //                             'title' => [
        //                                 'type' => 'string',
        //                                 'description' => '产品标题',
        //                                 'example' => '科罗娜皇家至尊系列'
        //                             ],
        //                             'status' => [
        //                                 'type' => 'integer',
        //                                 'description' => '产品状态',
        //                                 'example' => 1
        //                             ]
        //                         ]
        //                     ]
        //                 ]
        //             ]
        //         ]
        //     ]
        // ]
    }

    public static function getCatalog($path="")
    {
        $url = sprintf("%s/api/v1/projects/%s/api-folders?locale=zh-CN", self::$domain, self::$projectId);
        $result = self::get($url);
        if (empty($result['success']) || $result['success'] !== true) {
            return [
                'code' => '505',
                'message' => 'apifox获取目录接口请求失败'
            ];
        }

        $list = explode("/", $path);
        $catalog = self::createCatalog($list, 0, $result['data']);
        return $catalog;
    }

    public static function createCatalog($list, $parentId=0, $catalogs=[])
    {
        $id = 0;
        $name = $list[0];
        if (! empty($catalogs)) {
            foreach ($catalogs as $val) {
                if ($val['name'] == $name && $val['parentId'] == $parentId) {
                    $id = $val['id'];
                }
            }

            if ($id == 0) {
                $url = sprintf("%s/api/v1/projects/%s/api-folders", self::$domain, self::$projectId);
                $data = [
                    'name' => $name,
                    'parentId' => $parentId
                ];
                $result = self::post($url, $data, 'form');
                if (! empty($result['data'])) {
                    $id = $result['data']['id'];
                }
            }
        }
        if ($id == 0) {
            return [
                'code' => '505',
                'message' => "apifox创建 $name 目录失败"
            ];
        }

        unset($list[0]);
        if (count($list) == 0) {
            return [
                'code' => '0',
                'id' => $id
            ];
        }

        $list = array_values($list);
        return self::createCatalog($list, $id, $catalogs);
    }

    /**
     * @param $url
     * @param $data
     * @return array|mixed
     */
    private static function post($url, $data, $type="json")
    {
        $header = [
            "X-Apifox-Version: 2024-01-20",
            "Authorization: Bearer ". self::$authorization,
            "User-Agent: Apifox/1.0.0 (https://apifox.com)"
        ];
        if ($type == 'json') {
            $data = json_encode($data);
            $header[] = "Content-Type: application/json";
        } else if ($type == 'form') {
            $data = http_build_query($data);
            $header[] = "Content-Type: application/x-www-form-urlencoded";
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_NOSIGNAL, false);
        //设置头文件的信息作为数据流输出
        if (! empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HEADER, 0);//返回response头部信息
        }
        //设置超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        //设置post方式提交
        curl_setopt($ch, CURLOPT_POST, 1);
        //设置post数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);
        $errno = curl_errno($ch);
        $errorMsg = curl_error($ch);
        curl_close($ch);
        $result = json_decode($output, true);
        if (empty($result)) {
            $result = $output;
        }
        if ($errno) {
            $result = 'Error code:'.$errno.";message:".$errorMsg;
        }
        return $result;
    }

    private static function get($url)
    {
        $header = [
            "X-Apifox-Version: 2024-01-20",
            "Authorization: Bearer ". self::$authorization,
            "User-Agent: Apifox/1.0.0 (https://apifox.com)",
            "Content-Type: application/json"
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //设置头文件的信息作为数据流输出
        if (! empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HEADER, 0);//返回response头部信息
        }
        //设置超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        $output = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);
        $result = json_decode($output, true);
        return $result;
    }
}