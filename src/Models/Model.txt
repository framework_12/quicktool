<?php

namespace App\Models\%s\%s;

use App\Models\%s\BaseModel;
use Illuminate\Support\Facades\DB;
use App\Services\OSS;
use Illuminate\Support\Facades\Storage;
use App\Models\%s\ImagesManage;
use App\Http\Traits\FormatTrait;
use App\Common\Upload;
use App\Common\Helper;

class %s extends BaseModel
{
    use FormatTrait;
	public $table = "%s";

	//上传图片
	%s
	
	/**
	 * 列表查询
	*/
	public function listPage($params)
	{
	    $where = [];

        %s
        //order by
        $orderField = "id";
        $sort = "desc";
        if (! empty($params["order"])) {
            $order = explode("|", $params["order"]);
            $orderField = $order[0];
            $sort = str_replace("ending", "", $order[1]);
        }

        $data = $this->where($where)->orderBy($orderField, $sort)->paginate(20, ["*"], "page", $params["page"]);
        
        %s
        
        return [
            "total" => $data->total(),
            "list" => $items
        ];
	}
	
	/**
	 * 单个添加
	 */
	public function addOne($params)
	{
	    return $this->insertGetId($params);
	}
	
	/**
	 * 单个编辑
	 */
	public function editOne($params)
	{
	
	    return $this->where('id', $params['id'])->update($params);
	}
	
	/**
	 * 单个删除
	 */
	public function removeOne($params)
	{
	    %s
	    return $result;
	}

	/**
	 * 批量删除
	 */
	public function batchRemove($params)
	{
	    %s
	    return $result;
	}
}