	//上传图片
    public function upload($params, $checkExist = true){
        if (empty($params['file'])) {
            return ['code' => 10001, 'message' => '上传图片异常1'];
        }
        
        if (! $params['file']->isValid()) {
            return ['code' => 10001, 'message' => '上传图片异常2'];
        }
        
        try {
            $file = $params['file'];
            $path = $params['path'];
            $kuoname = $params['file']->getClientOriginalExtension();
            $fileName = date('YmdHis') . rand(10000, 99999) . '.' . $kuoname;
            
            $cUpload = new Upload();
            $res = $cUpload->uploadToTmp($file, $path, $fileName);
            if ($res === false) {
                return ['code' => 10001, 'message' => '上传图片异常3'];
            }

            return [
                'code' => 0,
                'path' => $res,
                'url_pre' => config('filesystems.disks.tmp.url')
            ];
        } catch (\Exception $e) {
            // return ['code' => 10001, 'message' => $e->getFile()."=>".$e->getLine()."=>".$e->getMessage()];
            return ['code' => 10001, 'message' => '上传图片异常4'];
        }
    }