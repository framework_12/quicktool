<?php

namespace Ymkj\Quicktool\Models;

use App\Models\Admin\BaseModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Ymkj\Quicktool\Config;

class ToolHelper extends BaseModel
{
    public function editMysql($params) {
        $tableName = strtolower($params['name']);
        
        $old = $this->getCreateTable($tableName);
        
        $columns = [];
        foreach ($params['struct'] as $struct) {
            if ($struct['name'] !== 'id') {
                $columns[] = $struct['name'];
            }
        }
        
        $oldColums = [];
        $oldByName = [];
        //删除字段
        $delColums = '';
        foreach ($old['list'] as $val) {
            $oldColums[] = $val['name'];
            $oldByName[$val['name']] = $val;
            
            if ($val['name'] !== 'id' && ! in_array($val['name'], $columns)) {
                $delColums .= sprintf("\$table->dropColumn('%s');\r\n", $val['name']);
            }
        }
        
        // 新增字段
        $addColums = '';
        $changeColums = '';
        // 变更字段
        $tableFeildToMysqlType = Config::get('tableFeildToMysqlType');
        foreach ($params['struct'] as $struct) {
            if ($struct['name'] !== 'id') {
                if (! in_array($struct['name'], $oldColums)) {
                    $tmp = $this->mysqlFieldFormat($struct);
                    $addColums .= $tmp . ";\r\n";
                } else {
                    $oldData = $oldByName[$struct['name']];
                    
                    if (
                        $struct['type'] !== $oldData['type']
                        || ( ! empty($struct['length']) && $struct['length'] !== $oldData['length'] )
                        || ( ! empty($struct['default']) && $struct['default'] !== "不可设置" && $struct['default'] !== $oldData['default'])
                        || $struct['nullable'] !== $oldData['nullable']
                        || $struct['comment'] !== $oldData['comment']
                    ) {
                        if (! empty($struct['length'])) {
                            $changeColum = sprintf("\$table->%s('%s', %s)", $struct['type'], $struct['name'], $struct['length']);
                        } else {
                            $changeColum = sprintf("\$table->%s('%s')", $struct['type'], $struct['name']);
                        }
    
                        if ($struct['default'] !== $oldByName[$struct['name']]['default']) {
                            $changeColum .= sprintf("->default('%s')", $struct['default']);
                        }
                        if ($struct['nullable'] !== $oldByName[$struct['name']]['nullable']) {
                            if ($struct['nullable']) {
                                $changeColum .= "->nullable()";
                            } else {
                                $changeColum .= "->nullable(false)";
                            }
                        }
                        if ($struct['comment'] !== $oldByName[$struct['name']]['comment']) {
                            $changeColum .= sprintf("->comment('%s')", $struct['comment']);
                        }
                        
                        $changeColums .= $changeColum."->change();\r\n";
                    }
                }
            }
        }
        // var_dump($changeColums);
        // exit();
        
        $index = [];
        foreach ($params['index'] as $val) {
            $index[] = $val['name'];
        }
        
        $oldIndex = [];
        // 删除索引
        $delIndex = '';
        foreach ($old['index'] as $val) {
            $oldIndex[] = $val['name'];
            if (! in_array($val['name'], $index)) {
                $delIndex .= sprintf("\$table->dropIndex('%s');\r\n", $val['name']);
            }
        }
        
        // 新增索引
        $addIndex = '';
        foreach ($params['index'] as $val) {
            if (! in_array($val['name'], $oldIndex)) {
                $columns = implode("','", $val['columns']);
                $tmp = sprintf("\$table->%s(['%s'], '%s');\r\n", $val['type'], $columns, $val['name']);

                $addIndex .= $tmp;
            }
        }
        
        $tableNameCamelCase = $this->toCamelCase($tableName);
        $time = date('Y_m_d_His');
        $time2 = date('YmdHis');
        $migrationFile = sprintf(base_path() . "/database/migrations/%s_alter_%s_table.php",
            $time,
            $tableName.$time2
        );
        
        $fileTpl = __dir__ . "/TableAlterTpl.txt";
        $tpl = file_get_contents($fileTpl);
        $content = sprintf($tpl,
            $tableNameCamelCase.$time2,
            $tableName,
            $delColums,
            $addColums,
            $changeColums,
            $delIndex,
            $addIndex,
            $tableName
        );

        file_put_contents($migrationFile, $content);
        chmod($migrationFile,0777);
        $out = shell_exec("/www/server/php/81/bin/php " . base_path() . "/artisan migrate");
        $success = sprintf("Migrated:  %s_alter_%s_table", $time, $tableName.$time2);
        if (preg_match("/$success/", $out)) {
            return true;
        } else {
            return "编辑数据表格失败".$out;
        }
    }
    
    public function createMysql($params) {
        $tableName = strtolower($params['name']);
        // 表名大驼峰写法
        $tableNameCamelCase = $this->toCamelCase($tableName);

        $time = date('Y_m_d_His');
        $migrationFile = sprintf(base_path() . "/database/migrations/%s_create_%s_table.php",
            $time,
            $tableName
        );

        $listHistory = scandir(base_path() . "/database/migrations");
        foreach ($listHistory as $val) {
            $matchTpl = sprintf("create_%s_table", $tableName);
            $pattern = "/$matchTpl/";
            if (preg_match($pattern, $val)) {
                if (! empty($params['force']) && $params['force'] == 1) {
                    @unlink(base_path() . "/database/migrations/$val"); //强制删除已存在的表迁移文件，并重新创建

                    $migrations = str_replace('.php', '', $val);
                    $migrationsExist = DB::table("migrations")->where("migrations", $migrations)->first();
                    if ($migrationsExist) {
                        DB::table("migrations")->where("migrations", $migrations)->delete();
                    }
                    
                    @DB::statement("DROP TABLE $tableName");
                } else {
                    return "该名称创建迁移文件已存在, 确认删除并重新创建\r\n$val";
                }
            }
        }

        $fieldStr = "";
        foreach ($params['struct'] as $val) {
            $tmp = $this->mysqlFieldFormat($val);
            $fieldStr .= $tmp . ";\r\n";
        }

        // 索引
        $indexStr = "";
        if (! empty($params['index'])) {
            foreach ($params['index'] as $val) {
                $columns = implode("','", $val['columns']);
                $tmp = sprintf("\$table->%s(['%s'], '%s');\r\n", $val['type'], $columns, $val['name']);

                $indexStr .= $tmp;
            }
        }

        $fileTpl = __dir__ . "/TableTpl.txt";
        $tpl = file_get_contents($fileTpl);
        $content = sprintf($tpl,
            $tableNameCamelCase,
            $tableName,
            $fieldStr,
            $indexStr,
            $tableName,
            $params['comment'],
            $tableName
        );

        file_put_contents($migrationFile, $content);
        chmod($migrationFile,0777);
        $out = shell_exec("/www/server/php/81/bin/php " . base_path() . "/artisan migrate");
        $success = sprintf("Migrated:  %s_create_%s_table", $time, $tableName);
        if (preg_match("/$success/", $out)) {
            return true;
        } else {
            return "创建数据表格失败".$out;
        }
    }
    
    /**
     * 格式化创建字段
     */
    private function mysqlFieldFormat($val)
    {
        if (! empty($val['length'])) {
            $tmp = sprintf("\$table->%s('%s', %s)", $val['type'], $val['name'], $val['length']);
        } else {
            $tmp = sprintf("\$table->%s('%s')", $val['type'], $val['name']);
        }

        if ($val['default'] !== '' && $val['default'] !== null && $val['default'] !== "不可设置") {
            $tmp .= sprintf("->default('%s')", $val['default']);
        }
        if ($val['nullable']) {
            $tmp .= "->nullable()";
        }
        if ($val['comment'] !== '') {
            $tmp .= sprintf("->comment('%s')", $val['comment']);
        }
        
        return $tmp;
    }

    public function getConfig()
    {
        $tableList = [];
        $tables = DB::select('SHOW TABLES');
        foreach ($tables as $table) {
            $tableName = $table->{'Tables_in_' . env('DB_DATABASE')};
            if (! in_array($tableName, ['migrations', 'permissions', 'roles', 'users', 'logs'])) {
                $tableList[] = $table->{'Tables_in_' . env('DB_DATABASE')};
            }
        }
        return $tableList;
    }

    public function getCreateTable($tableName)
    {
        $tableFeildToMysqlType = Config::get('tableFeildToMysqlType');

        $data = [
            'tableComment' => '',
            'list' => [],
            'index' => []
        ];
        $tableArr = DB::select(sprintf('SHOW CREATE TABLE %s', $tableName));
        $tableInfo = $tableArr[0]->{'Create Table'};

        // 表注释
        $pattern = "/COMMENT='.*?'/";
        preg_match($pattern, $tableInfo, $tableCommentMatch);
        if (empty($tableCommentMatch)) {
            $data['tableComment'] = '';
        } else {
            $data['tableComment'] = str_replace(["COMMENT='", "'"], [], $tableCommentMatch[0]);
        }

        // 字段
        $fields = [];
        $tmpArr = explode("\n", $tableInfo);
        $tmpLen = count($tmpArr);
        unset($tmpArr[$tmpLen - 1]);
        unset($tmpArr[0]);
        if (! empty($tmpArr)) {
            foreach ($tmpArr as $val) {
                // 索引
                preg_match("/ KEY /",  $val, $keyMatch);
                if (! empty($keyMatch)) {
                    $val = trim($val, ",");
                    $keyTmpArr = explode(" KEY ", $val);
                    $keyTmpArr[0] = trim($keyTmpArr[0]);
                    if ($keyTmpArr[0] !== "PRIMARY") {
                        $columns = explode(",", str_replace([")", "`"], [], explode("(", $keyTmpArr[1])[1]));
                        $name = trim(explode("(", $keyTmpArr[1])[0]);
                        
                        $data['index'][] = [
                            'columns' => $columns,
                            'type' => empty($keyTmpArr[0])? 'INDEX': $keyTmpArr[0],
                            'name' => str_replace(["`"], [], $name)
                        ];
                    }
                }
                
                // 字段
                $pattern = "/^  `/";
                preg_match($pattern, $val, $match);
                if (! empty($match)) {
                    preg_match("/DEFAULT '.*?'/", $val, $defaultMatch);
                    $default = '';
                    if (! empty($defaultMatch)) {
                        $default = str_replace(["DEFAULT '", "'"], [], $defaultMatch[0]);
                    }
                    
                    $nullable = true;
                    if (preg_match("/NOT NULL/", $val)) {
                        $nullable = false;
                    }
                    
                    $val = str_replace(["  `", "`"], ["", ""], $val);
                    $val = explode(" ", $val);

                    $fieldComment = '';
                    $p = "/'.*?',/";
                    $end = end($val);
                    if (preg_match($p, $end, $endMatch)) {
                        $fieldComment = str_replace(["'", ","], [], $endMatch[0]);
                    } else if ($val[0] === 'created_at') {
                        $fieldComment = "创建时间";
                    } else if ($val[0] === 'updated_at') {
                        $fieldComment = "更新时间";
                    }

                    $typeArr = explode("(", $val[1]);
                    $type = $tableFeildToMysqlType[$typeArr[0]];
                    $length = '';
                    if (! empty($typeArr[1])) {
                        $length = str_replace(")", "", $typeArr[1]);
                    }

                    $data['list'][] = [
                        'name' => $val[0],
                        'type' => $type,
                        'length' => $length,
                        'nullable' => $nullable,
                        'comment' => $fieldComment,
                        'default' => $default
                    ];
                }
            }
        }

        return $data;
    }

    public function refreshAdmin($params)
    {
        try {
            $config = config($params['client_type']);
            $configFile = base_path(). sprintf("/config/%s.php", $params['client_type']);
            if (! array_key_exists($params['parent'], $config['nav'])) {
                $adminContent = file_get_contents($configFile);
                $adminArr = explode("//nav", $adminContent);
                $adminArr[1] .= sprintf("'%s' => ['sort' => %d, 'alias' => '%s'],  // 必须 ',' 结尾\r\n        ", $params['parent'], count($config['nav']) + 1, $params['parentName']);
                $adminContent = implode("//nav", $adminArr);
                chmod($configFile, 0777);
                file_put_contents($configFile, $adminContent);
                chmod($configFile, 0777);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'code' => 10001,
                'message' => $e->getFile() ." ". $e->getLine() ." ". $e->getMessage()
            ];
        }
        return [
            'code' => 0
        ];
    }

    public function mkController($params)
    {
        try {
            $config = config($params['client_type']);
            $clientType = ucfirst($params['client_type']);
            $apiSubName = $this->apiSubNameFormat($params['path']);

            $path = base_path(). sprintf("/app/Http/Controllers/%s/%s", $clientType, $params['parent']);
            if (! is_dir($path)) {
                mkdir($path,0777,true);
            }

            if (file_exists(sprintf("%s/%sController.php", $path, ucfirst($params['path'])))) {
                throw new \Exception("Controller文件已存在： " . sprintf("%s/%sController.php", $path, ucfirst($params['path'])));
            }

            $tpl = file_get_contents(__dir__. "/Controller.txt");

            $upload = "";
            $is_upload = 0;
            if (! empty($params['fields'])) {
                foreach ($params['fields'] as $field) {
                    if (! empty($field['formFields'])) {
                        if (in_array($field['formType'],["Image","Multi Image","Quill"]) && $is_upload == 0) { // 上传图片
                            $upload .= sprintf("/**\r\n        ");
                            $upload .= sprintf(" * @name 上传图片\r\n        ");
                            $upload .= sprintf(" * @Post('/%s/%s/%s/upload')\r\n        ", $config['api_prefix'], strtolower($params['parent']), $apiSubName);
                            $upload .= sprintf(" * @Versions('v1')\r\n        ");
                            $upload .= sprintf("*/\r\n        ");
                            $upload .= sprintf("public function upload(Request \$request, %s \$m%s)\r\n        ",ucfirst($apiSubName),ucfirst($apiSubName));
                            $upload .= sprintf("{\r\n        ");
                            $upload .= sprintf("  \$params['file'] = \$request->file('file');\r\n        ");
                            $upload .= sprintf("  \$params['path'] = '%s/';\r\n        ",$apiSubName);
                            $upload .= sprintf("  \$result = \$m%s->upload(\$params);\r\n        ",ucfirst($apiSubName));
                            $upload .= sprintf("  if (\$result) {\r\n        ");
                            $upload .= sprintf("      return \$this->json%sResultWithLog(\$request,\$result);\r\n        ", $clientType);
                            $upload .= sprintf("  } else {\r\n        ");
                            $upload .= sprintf("      return \$this->json%sResultWithLog(\$request, [], 10001);\r\n        ", $clientType);
                            $upload .= sprintf("  }\r\n        ");
                            $upload .= sprintf("}\r\n        ");
                            $is_upload = 1;
                        }
                    }
                }
            }

            $controller = sprintf($tpl,
                $clientType,
                $params['parent'],
                $clientType,
                $clientType,
                $params['parent'],
                ucfirst($apiSubName),
                $params['name'],
                $clientType,
                $params['parent'],
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                //listPage
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                $clientType,
                //addOne
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                $clientType,
                $clientType,
                //editOne
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                $clientType,
                $clientType,
                //removeOne
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                $clientType,
                $clientType,
                //batchRemove
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                ucfirst($apiSubName),
                $clientType,
                $clientType,
                //upload
                $upload,
            );

            file_put_contents(sprintf("%s/%sController.php", $path, ucfirst($apiSubName)), $controller);
            chmod(sprintf("%s/%sController.php", $path, ucfirst($apiSubName)),0777);
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'code' => 10001,
                'message' => $e->getMessage()
            ];
        }
        return [
            'code' => 0
        ];
    }

    public function mkModel($params)
    {
        try {
            $clientType = ucfirst($params['client_type']);
            
            $apiSubName = $this->apiSubNameFormat($params['path']);
            $path = base_path(). sprintf("/app/Models/%s/%s", $clientType, $params['parent']);
            if (! is_dir($path)) {
                mkdir($path, 0777, true);
                chmod($path,0777);
            }

            if (file_exists(sprintf("%s/%s.php", $path, ucfirst($apiSubName)))) {
                throw new \Exception("Models 文件已存在： " . sprintf("%s/%s.php", $path, ucfirst($apiSubName)));
            }

            $where = "";
            if (Schema::hasColumn($params['path'], 'is_del')) {
                $where .= sprintf("\$where[] = ['is_del', '=', 0];\r\n        ");
            }

            $upload = "";
            $is_upload = 0;
            $listPageNeedFor = false; // 列表查询需要for循环处理字段
            $listPageImg = "";
            if (! empty($params['fields'])) {
                foreach ($params['fields'] as $field) {
                    if (! empty($field['searchFields'])) {
                        if ($field['searchType'] === "DateTime Picker") { // 日期筛选
                            $where .= sprintf("if (! empty(\$params['%s'])) {\r\n        ", $field['name']);
                            $where .= sprintf("  \$where[] = ['%s', '>=', \$params['%s'][0]];\r\n        ", $field['name'], $field['name']);
                            $where .= sprintf("  \$where[] = ['%s', '<=', \$params['%s'][1]];\r\n        ", $field['name'], $field['name']);
                            $where .= sprintf("}\r\n        ");
                        } else if ($field['searchType'] === "Input") {
                            $where .= sprintf("if (! empty(\$params['%s'])) {\r\n        ", $field['name']);
                            $where .= "  \$where[] = ['" . $field['name'] . "', 'like', \"%\".\$params['". $field['name'] ."'].\"%\"];\r\n        ";
                            $where .= sprintf("}\r\n        ");
                        } else {
                            // var_dump($field);
                            $where .= sprintf("if (! empty(\$params['%s'])) {\r\n        ", $field['name']);
                            $where .= sprintf("  \$where[] = ['%s', '=', \$params['%s']];\r\n        ", $field['name'], $field['name']);
                            $where .= sprintf("}\r\n        ");
                        }
                    }
                    
                    if (! empty($field['formFields'])) {
                        if (in_array($field['formType'], ["Image", "Multi Image", "Quill"]) && $is_upload == 0) { // 上传图片
                            $upload .= file_get_contents(__dir__. "/ModelUpload.txt");
                            $is_upload = 1;
                        }
                        
                        if ($field['formType'] == "Image" || $field['formType'] == "Multi Image") {
                            $listPageNeedFor = true;
                            $listPageImg .= sprintf("                \$%s = json_decode(\$v['%s'], true);\r\n", $field['name'], $field['name']);
                            $listPageImg .= sprintf("                \$%sSrc= [];\r\n", $field['name']);
                            $listPageImg .= sprintf("                if (! empty(\$%s)) {\r\n", $field['name']);
                            $listPageImg .= sprintf("                    foreach (\$%s as \$val) {\r\n", $field['name']);
                            $listPageImg .= sprintf("                        \$%sSrc[] = sprintf('%s/%s', \$urlPre, \$val['name']);\r\n", $field['name'], "%s", "%s");
                            $listPageImg .= sprintf("                    }\r\n");
                            $listPageImg .= sprintf("                }\r\n");
                            $listPageImg .= sprintf("                \$items[\$k]['%s'] = \$%sSrc;\r\n", $field['name'], $field['name']);
                        }
                    }
                }
            }
            
            $removeOne = "";
            if (Schema::hasColumn($params['path'], 'is_del')) {
                $removeOne .= sprintf("\$data['is_del'] = 1;\r\n        ");
                $removeOne .= sprintf("\$data['updated_at'] = date('Y-m-d H:i:s');\r\n        ");
                $removeOne .= sprintf("\$result = \$this->where('id', \$params['id'])->update(\$data);\r\n        ");
            } else {
                $removeOne .= sprintf("\$result = \$this->where('id', \$params['id'])->delete();\r\n        ");
            }

            $batchRemove = "";
            if (Schema::hasColumn($params['path'], 'is_del')) {
                $batchRemove .= sprintf("\$data['is_del'] = 1;\r\n        ");
                $batchRemove .= sprintf("\$data['updated_at'] = date('Y-m-d H:i:s');\r\n        ");
                $batchRemove .= sprintf("\$result = \$this->whereIn('id', \$params['ids'])->update(\$data);\r\n        ");
            } else {
                $batchRemove .= sprintf("\$result = \$this->whereIn('id', \$params['ids'])->delete();\r\n        ");
            }

            $forItems = "";
            if ($listPageNeedFor) {
                if (! empty($listPageImg)) { // 列表查询需要拼接图片地址
                    $forItems .= sprintf("        \$urlPre = Helper::getImgDomain();\r\n");
                }
                
                // 公用
                $forItems .= sprintf("        \$items = \$this->dbResult(\$data->items());\r\n");
                $forItems .= sprintf("        if (! empty(\$items)) {\r\n");
                $forItems .= sprintf("            foreach (\$items as \$k => \$v) {\r\n");
                
                $forItems .= $listPageImg;
                
                $forItems .= sprintf("            }\r\n");
                $forItems .= sprintf("        }\r\n");
            }
            
            $tpl = file_get_contents(__dir__. "/Model.txt");
            $model = sprintf($tpl,
                $clientType,
                $params['parent'],
                $clientType,
                $clientType,
                ucfirst($apiSubName),
                $params['path'],
                $upload,
                
                //listPage
                $where,
                $forItems,
                //editOne
                
                //removeOne
                $removeOne,
                //batchRemove
                $batchRemove
            );

            file_put_contents(sprintf("%s/%s.php", $path, ucfirst($apiSubName)), $model);
            chmod(sprintf("%s/%s.php", $path, ucfirst($apiSubName)),0777);
        } catch (\Exception $e){
            return [
                'code' => 10001,
                'message' => $e->getMessage()
            ];
        }
        return [
            'code' => 0
        ];
    }

    public function refreshApi($params) {
        $clientType = ucfirst($params['client_type']);
        if ($clientType == 'Admin') {
            return $this->refreshApiByAdmin($params);
        } else {
            return $this->refreshApiByOther($params); //自定义
        }
    }

    private function refreshApiByOther($params) {
        try{
            $config = config($params['client_type']);
            $clientType = ucfirst($params['client_type']);
            $apiFile = sprintf(base_path(). "/routes/%s.php", $params['client_type']);
            
            $tpl = file_get_contents($apiFile);
            $tplArr = explode("///////// 权限认证api /////////", $tpl);

            $apiSubName = $this->apiSubNameFormat($params['path']);

            // 列表
            $newApi = sprintf("\r\n    //%s\r\n", $params['name']);
            $newApi .= sprintf("    Route::get('%s/%s/%s', '\App\Http\Controllers\%s\%s\%sController@listPage');\r\n",
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                $clientType,
                $params['parent'],
                ucfirst($apiSubName)
            );
            // 增加
            $newApi .= sprintf("    Route::post('%s/%s/%s/addOne', '\App\Http\Controllers\%s\%s\%sController@addOne');\r\n",
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                $clientType,
                $params['parent'],
                ucfirst($apiSubName)
            );
            // 编辑
            $newApi .= sprintf("    Route::post('%s/%s/%s/editOne', '\App\Http\Controllers\%s\%s\%sController@editOne');\r\n",
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                $clientType,
                $params['parent'],
                ucfirst($apiSubName)
            );
            // 单个删除
            $newApi .= sprintf("    Route::post('%s/%s/%s/removeOne', '\App\Http\Controllers\%s\%s\%sController@removeOne');\r\n",
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                $clientType,
                $params['parent'],
                ucfirst($apiSubName)
            );
            // 批量删除
            $newApi .= sprintf("    Route::post('%s/%s/%s/batchRemove', '\App\Http\Controllers\%s\%s\%sController@batchRemove');\r\n",
                $config['api_prefix'],
                strtolower($params['parent']),
                $apiSubName,
                $clientType,
                $params['parent'],
                ucfirst($apiSubName)
            );
            $is_upload = 0;
            if (! empty($params['fields'])) {
                foreach ($params['fields'] as $field) {
                    if (! empty($field['formFields'])) {
                        if (in_array($field['formType'],["Image","Multi Image","Quill"]) && $is_upload == 0) { // 上传图片
                            $newApi .= sprintf("    Route::post('%s/%s/%s/upload', '\App\Http\Controllers\%s\%s\%sController@upload');\r\n",
                                $config['api_prefix'],
                                strtolower($params['parent']),
                                $apiSubName,
                                $clientType,
                                $params['parent'],
                                ucfirst($apiSubName)
                            );
                            $is_upload = 1;
                        }
                    }
                }
            }


            $tplArr[1] = $newApi . $tplArr[1];
            $tplContent = implode("///////// 权限认证api /////////", $tplArr);

            file_put_contents($apiFile, $tplContent);
            chmod($apiFile, 0777);
            
            return [
                'code' => 0
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'code' => 10001,
                'message' => $e->getMessage()
            ];
        }
    }
    
    private function refreshApiByAdmin($params) {
        try{
            $tpl = file_get_contents(base_path(). "/routes/api.php");
            $tplArr = explode("///////// 权限认证api /////////", $tpl);

            $apiSubName = $this->apiSubNameFormat($params['path']);

            $newApi = sprintf("\r\n    //%s\r\n", $params['name']);
            $newApi .= sprintf("    \$dingoApi->get('%s/%s', \App\Http\Controllers\Admin\%s\%sController::class.'@listPage')->name('%s.%s.listPage');\r\n",
                strtolower($params['parent']),
                $apiSubName,
                $params['parent'],
                ucfirst($apiSubName),
                strtolower($params['parent']),
                $apiSubName,
            );
            $newApi .= sprintf("    \$dingoApi->post('%s/%s/addOne', \App\Http\Controllers\Admin\%s\%sController::class.'@addOne')->name('%s.%s.addOne');\r\n",
                strtolower($params['parent']),
                $apiSubName,
                $params['parent'],
                ucfirst($apiSubName),
                strtolower($params['parent']),
                $apiSubName,
            );
            $newApi .= sprintf("    \$dingoApi->post('%s/%s/editOne', \App\Http\Controllers\Admin\%s\%sController::class.'@editOne')->name('%s.%s.editOne');\r\n",
                strtolower($params['parent']),
                $apiSubName,
                $params['parent'],
                ucfirst($apiSubName),
                strtolower($params['parent']),
                $apiSubName,
            );
            $newApi .= sprintf("    \$dingoApi->post('%s/%s/removeOne', \App\Http\Controllers\Admin\%s\%sController::class.'@removeOne')->name('%s.%s.removeOne');\r\n",
                strtolower($params['parent']),
                $apiSubName,
                $params['parent'],
                ucfirst($apiSubName),
                strtolower($params['parent']),
                $apiSubName,
            );
            $newApi .= sprintf("    \$dingoApi->post('%s/%s/batchRemove', \App\Http\Controllers\Admin\%s\%sController::class.'@batchRemove')->name('%s.%s.batchRemove');\r\n",
                strtolower($params['parent']),
                $apiSubName,
                $params['parent'],
                ucfirst($apiSubName),
                strtolower($params['parent']),
                $apiSubName,
            );
            $is_upload = 0;
            if (! empty($params['fields'])) {
                foreach ($params['fields'] as $field) {
                    if (! empty($field['formFields'])) {
                        if (in_array($field['formType'],["Image","Multi Image","Quill"]) && $is_upload == 0) { // 上传图片
                            $newApi .= sprintf("    \$dingoApi->post('%s/%s/upload', \App\Http\Controllers\Admin\%s\%sController::class.'@upload')->name('%s.%s.upload');\r\n",
                                strtolower($params['parent']),
                                $apiSubName,
                                $params['parent'],
                                ucfirst($apiSubName),
                                strtolower($params['parent']),
                                $apiSubName,
                            );
                            $is_upload = 1;
                        }
                    }
                }
            }


            $tplArr[1] = $newApi . $tplArr[1];
            $tplContent = implode("///////// 权限认证api /////////", $tplArr);

            file_put_contents(base_path(). "/routes/api.php", $tplContent);
            chmod(base_path(). "/routes/api.php",0777);
            
            return [
                'code' => 0
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'code' => 10001,
                'message' => $e->getMessage()
            ];
        }
    }

    // 字符串转大驼峰命名
    private function toCamelCase($str) {
        $str = ucwords($str);
        return str_replace("_", "", $str);
    }

    private function apiSubNameFormat($str) {
        $arr = explode("_", $str);

        $newStr = "";
        foreach ($arr as $k => $val) {
            $newStr .= $k == 0 ? strtolower($val): ucfirst($val);
        }

        return $newStr;
    }
}