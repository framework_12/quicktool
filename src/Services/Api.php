<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which 
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/tools/getConfig', 'Ymkj\Quicktool\Controllers\IndexController@getConfig');
Route::post('/tools/createMysql', 'Ymkj\Quicktool\Controllers\IndexController@createMysql');
Route::post('/tools/editMysql', 'Ymkj\Quicktool\Controllers\IndexController@editMysql');
Route::get('/tools/getTable', 'Ymkj\Quicktool\Controllers\IndexController@getTable');
Route::post('/tools/mkApiServer', 'Ymkj\Quicktool\Controllers\IndexController@mkApiServer');
