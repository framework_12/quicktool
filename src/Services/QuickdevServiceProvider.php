<?php
namespace Ymkj\Quicktool\Services;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class QuickdevServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // 注册路由
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/Api.php');
        });
    }

    protected function routeConfiguration()
    {
        return [
            'prefix' => 'lv', // 根据需要设置前缀
            'middleware' => 'CrossHttp', // 根据需要设置中间件
        ];
    }
}